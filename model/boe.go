package model

// Boe the core that loads everything
type Boe interface {
	GetPluginConfig(id PluginID) map[string]string
	GetVersion() string
	SaveConfig() error
	SetPluginConfig(id PluginID, data map[string]string)
}
