package boe

import (
	"testing"
)

func TestEvents(t *testing.T) {
	b := getTestStruct()
	b.Start()
	if b.plugins["test"] == nil || !b.plugins["test"].(*testPlugin).enabled {
		t.Error("Failed to load test plugin")
	}

	b.Stop()
	if b.plugins["test"].(*testPlugin).enabled {
		t.Error("Failed to unload test plugin")
	}
	testCleanup()
}
