package boe

import (
	"log"

	"gitlab.com/parkermc/boe/model"
)

// Boe the base for the server
type Boe struct {
	Config
	PluginLoader

	version string
}

// New creates a new server struct
func New(configFile string, version string, internalPlugins ...model.Plugin) *Boe {
	b := &Boe{
		Config: Config{
			ConfigFilePath: configFile,
		},
		PluginLoader: PluginLoader{
			internalPlugins: make(map[model.PluginID]model.Plugin),
			plugins:         make(map[model.PluginID]model.Plugin),
		},
		version: version,
	}
	// Set the unloaded plugins
	for _, plugin := range internalPlugins {
		b.internalPlugins[plugin.GetInfo().ID] = plugin
	}

	return b
}

// GetVersion returns the version of BoE
func (b *Boe) GetVersion() string {
	return b.version
}

// Start starts the server
func (b *Boe) Start() {
	err := b.LoadConfig()
	if err != nil {
		log.Fatal(err)
	}
	b.enableEvent()
}

// Stop stops the server
func (b *Boe) Stop() {
	b.disableEvent()
}
